<?php
/**
 * This file is part of the EdiParser package.
 *
 * @package     EdiParserBundle
 * @since       0.0.1
 * @author      davidbonachera
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Boda\EdiParserBundle\Parser;

use Boda\EdiParserBundle\Model\ModelAbstract;

class Writer
{
    /**
     * Model
     *
     * @var ModelAbstract
     */
    private $model;

    public function __construct(ModelAbstract $model)
    {
        $this->setModel($model);
    }

    /**
     * Create & Write the whole positional file line by line and save it to a path
     * 1 - Create a temp file
     * 2 - Rename it to the original file (protection against regex file picking at the same time)
     *
     * @param string $path
     * @param string $customEncoding
     *
     * @return boolean
     */
    public function writeFile($path, $customEncoding = '')
    {
        // @TODO Check if the last character of path is a "/"
        $tempname = $path."TMPEDI.txt";
        $file = fopen($tempname, "w") or die("Unable to open/create file!");
        // Write Header
        $str = $this->writeLine($this->model->getHeader(), $this->model->getValidationTemplateHeader(), $customEncoding)."\n";
        fwrite($file, $str);
        $data = $this->model->getData();
        // Write Data
        foreach ($data as $lineKey => $lineData) {
            // ToDo : Fix and find a way to identify the header in a proper way (cf end of line)
            $str = $this->writeLine(
                    $lineData,
                    $this->model->getValidationTemplateData()[$lineData["CODEXC"].$lineData["SEPEXC"].$lineData["SCOEXC"]],
                    $customEncoding
                )."\n";
            fwrite($file, $str);
        }
        // Write Footer
        $str = $this->writeLine($this->model->getFooter(), $this->model->getValidationTemplateFooter(), $customEncoding)."\n";
        fwrite($file, $str);
        $filename = $path.$this->getModel()->__toString().date("YmdHis").".txt";
        if (file_exists($filename)) {
            $existingFile = fopen($filename, 'a');
            fwrite($existingFile, file_get_contents($tempname));
            fclose($existingFile);
            unlink($tempname);
        } else {
            rename($tempname, $filename);
        }
        fclose($file);

        return true;
    }

    /**
     * Parse a key-value array (a line to write) according to a template and return a positional string
     *
     * @param array  $line
     * @param array  $validationTemplate
     * @param string $customEncoding
     *
     * @return string
     */
    public function writeLine($line, $validationTemplate, $customEncoding = '')
    {
        $str = "";
        $pos = 0;
        $i = 0;
        foreach ($line as $key => $value) {
            $i++;
            if (is_null($value)) {
                if (empty($validationTemplate[$key]["numerical"])) {
                    $value = str_repeat(" ", $validationTemplate[$key]["length"]);
                } else {
                    $value = str_repeat("0", $validationTemplate[$key]["length"]);
                }

            } else {
                $offset = $validationTemplate[$key]["length"] - strlen($value);
                if ($offset > 0) {
                    if (!empty($validationTemplate[$key]["numerical"])) {
                        $value = str_repeat("0", $offset).$value;
                    } else {
                        $value = $value.str_repeat(" ", $validationTemplate[$key]["length"] - strlen($value));
                    }
                } elseif ($offset < 0) {
                    // @ToDo: Log error to say it's too long
                    $value = substr($value, 0, $validationTemplate[$key]["length"]);
                }
                if (empty($validationTemplate[$key]["preserveCase"])) {
                    $value = strtoupper($value);
                }
            }
            $str = substr_replace($str, $value, $pos, 0);
            $pos += $validationTemplate[$key]["length"];
        };

        if (!empty($customEncoding)) {
            $encodedStr = iconv('UTF-8', $customEncoding, $str);
            if ($encodedStr) {
                return $encodedStr;
            }
        }

        return $str;
    }

    /**
     * @return ModelAbstract
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param ModelAbstract $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * Get the default body/data template
     *
     * @return array
     */
    public function getTemplateData()
    {
        return $this->model->getTemplateData();
    }

    /**
     * Set default header value.
     *
     * @param array  $data An array of data to override default value
     * @param string $modelName
     */
    public function setHeader($data = [], $modelName = null)
    {
        $now = new \DateTimeImmutable();
        $modelName = $modelName ? $modelName : $this->getModel()->__toString();
        $default = $this->getTemplateHeader();
        $default["TRTEXC"] = '1';
        $default["CODACT"] = $data['CODACT'] ?? '';
        $default["BIBDTQ"] = $data['BIBDTQ'] ?? '';
        $default["EMTEXC"] = $data['EMTEXC'] ?? '';
        $default["DATEXC"] = $now->format('Ymd');
        $default["HEUEXC"] = $now->format('His');
        $default["RCTEXC"] = 'DATAQ '.$modelName;
        $default["NOMDTQ"] = $modelName;
        $default["LIBEXC"] = isset($data['LIBEXC']) ? $modelName.'|'.$data['LIBEXC'] : $modelName.'|'.$now->format('m/d|H:i:s').'|'.($data['CODACT'] ?? '');
        $this->model->setHeader($default);
    }

    /**
     * Get the default header template
     *
     * @return array
     */
    public function getTemplateHeader()
    {
        return $this->model->getTemplateHeader();
    }

    /**
     * Insert one line to the data model
     *
     * @param array  $data    ,
     * @param string $dataKey - Validation key to use when inserting the data @ToDo maybe its useless
     */
    public function insertOne(array $data, $dataKey)
    {
        $this->model->insertOne($data, $dataKey);
    }

    /**
     * Set default footer value.
     *
     * @param array  $data An array of data to override default value
     * @param string $modelName
     */
    public function setFooter($data = [], $modelName = null)
    {
        $now = new \DateTimeImmutable();
        $modelName = $modelName ? $modelName : $this->getModel()->__toString();
        $default = $this->getTemplateFooter();
        $default["TRTEXC"] = '1';
        $default["DATEXC"] = $now->format('Ymd');
        $default["HEUEXC"] = $now->format('His');
        $default["RCTEXC"] = 'DATAQ '.$modelName;
        $default["CPTEXC"] = count($this->getModel()->getData());
        $this->model->setFooter($default);
    }

    /**
     * Get the default footer template
     *
     * @return array
     */
    public function getTemplateFooter()
    {
        return $this->model->getTemplateFooter();
    }

    /**
     * WIP, for next version to override default data.
     *
     * @param array $arr1
     * @param array $arr2
     */
    function arrayMergeIfNotNull($arr1, $arr2)
    {
        foreach ($arr2 as $key => $val) {
            $is_set_and_not_null = isset($arr1[$key]);
            if ($val == null && $is_set_and_not_null) {
                $arr2[$key] = $arr1[$key];
            }
        }

        return array_merge($arr1, $arr2);
    }
}
