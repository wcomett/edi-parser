<?php
/**
 * This file is part of the EdiParser package.
 *
 * @package     EdiParserBundle
 * @since       0.0.1
 * @author      stevenkok
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Boda\EdiParserBundle\Model\Infolog;

use Boda\EdiParserBundle\Model\ModelAbstract;

class M80 extends ModelAbstract
{
    /**
     * Template Data
     *
     * @var array $templateData
     */
    public static $templateData = [
        // Déclaration produit (obligatoire)
        "80.00" => [
            "CODEXC" => "80",
            "SEPEXC" => ".",
            "SCOEXC" => "00",
            "TRTEXC" => null,
            "DATMVT" => null,
            "HEUMVT" => null,
            "DATRGL" => null,
            "CODMDU" => null,
            "CODFON" => null,
            "CODMVT" => null,
            "SENMVT" => null,
            "MOTMVT" => null,
            "EDIMVT" => null,
            "REFMVT" => null,
            "UVCMVT" => null,
            "CODACT" => null,
            "CODCLI" => null,
            "CODPRO" => null,
            "VALPRO" => null,
            "CODPRN" => null,
            "SPCPRO" => null,
            "PCBPRO" => null,
            "CODSIT" => null,
            "ZONSTS" => null,
            "ALLSTS" => null,
            "DPLSTS" => null,
            "NIVSTS" => null,
            "CODLOT" => null,
            "NUMLOT" => null,
            "CODPAL" => null,
            "DATFVI" => null,
            "NUMDIM" => null,
            "CODTIE" => null,
            "TYPTIE" => null,
            "NUMCDE" => null,
            "SNUCDE" => null,
            "CODUTI" => null,
            "UNIPRO" => null,
        ],
    ];
    public static $validationTemplateData = [
        "80.00" => [
            "CODEXC" => ["length" => 2, "required" => false],
            "SEPEXC" => ["length" => 1, "required" => false],
            "SCOEXC" => ["length" => 2, "required" => false],
            "TRTEXC" => ["length" => 1, "required" => false],
            "DATMVT" => ["length" => 8, "required" => false],
            "HEUMVT" => ["length" => 6, "required" => false],
            "DATRGL" => ["length" => 8, "required" => false],
            "CODMDU" => ["length" => 2, "required" => false],
            "CODFON" => ["length" => 1, "required" => false],
            "CODMVT" => ["length" => 2, "required" => false],
            "SENMVT" => ["length" => 1, "required" => false],
            "MOTMVT" => ["length" => 3, "required" => false],
            "EDIMVT" => ["length" => 3, "required" => false],
            "REFMVT" => ["length" => 30, "required" => false],
            "UVCMVT" => ["length" => 9, "required" => false],
            "CODACT" => ["length" => 3, "required" => false],
            "CODCLI" => ["length" => 14, "required" => false],
            "CODPRO" => ["length" => 17, "required" => false],
            "VALPRO" => ["length" => 2, "required" => false],
            "CODPRN" => ["length" => 17, "required" => false],
            "SPCPRO" => ["length" => 4, "required" => false],
            "PCBPRO" => ["length" => 5, "required" => false],
            "CODSIT" => ["length" => 3, "required" => false],
            "ZONSTS" => ["length" => 1, "required" => false],
            "ALLSTS" => ["length" => 3, "required" => false],
            "DPLSTS" => ["length" => 4, "required" => false],
            "NIVSTS" => ["length" => 2, "required" => false],
            "CODLOT" => ["length" => 20, "required" => false],
            "NUMLOT" => ["length" => 9, "required" => false],
            "CODPAL" => ["length" => 18, "required" => false],
            "DATFVI" => ["length" => 8, "required" => false],
            "NUMDIM" => ["length" => 8, "required" => false],
            "CODTIE" => ["length" => 14, "required" => false],
            "TYPTIE" => ["length" => 1, "required" => false],
            "NUMCDE" => ["length" => 8, "required" => false],
            "SNUCDE" => ["length" => 3, "required" => false],
            "CODUTI" => ["length" => 10, "required" => false],
            "UNIPRO" => ["length" => 3, "required" => false],
        ],
    ];

    public function getTemplateData()
    {
        return static::$templateData;
    }

    public function getValidationTemplateData()
    {
        return static::$validationTemplateData;
    }

    public function __toString()
    {
        return "M80";
    }
}
