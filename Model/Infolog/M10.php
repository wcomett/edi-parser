<?php
/**
 * This file is part of the EdiParser package.
 *
 * @package     EdiParserBundle
 * @since       0.0.1
 * @author      davidbonachera
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Boda\EdiParserBundle\Model\Infolog;

use Boda\EdiParserBundle\Model\ModelAbstract;

class M10 extends ModelAbstract
{
    /**
     * Template Data
     *
     * @var array $templateData
     */
    public static $templateData = [
        // Déclarations clients (obligatoire)
        "10.00" => [
            "CODEXC" => "10",
            "SEPEXC" => ".",
            "SCOEXC" => "00",
            "TRTEXC" => null,
            "CODACT" => null,
            "CODFOU" => null,
            "TYPFOU" => null,
            "NOMFOU" => null,
            "AD1FOU" => "AD1FOU",
            "AD2FOU" => null,
            "CPOFOU" => null,
            "VILFOU" => "VILFOU",
            "PAYFOU" => null,
            "TELFOU" => null,
            "FAXFOU" => null,
            "TLXFOU" => null,
            "EDIFOU" => null,
            "CODTRA" => null,
            "IGLSIT" => null,
            "CODTRE" => null,
            "GSTAVI" => null,
            "GSTRCP" => null,
            "TYPPRT" => null,
            "CPIFOU" => null,
            "EMBTIE" => null,
            "DISEXC" => null,
        ],
    ];

    public static $validationTemplateData = [
        "10.00" => [
            "CODEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "SEPEXC" => ["length" => 1, "required" => true],
            "SCOEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "TRTEXC" => ["length" => 1, "required" => true],
            "CODACT" => ["length" => 3, "required" => false],
            "CODFOU" => ["length" => 14, "required" => true],
            "TYPFOU" => ["length" => 1, "required" => false],
            "NOMFOU" => ["length" => 30, "required" => true],
            "AD1FOU" => ["length" => 30, "required" => true],
            "AD2FOU" => ["length" => 30, "required" => false],
            "CPOFOU" => ["length" => 5, "required" => false, "numerical" => true],
            "VILFOU" => ["length" => 26, "required" => true],
            "PAYFOU" => ["length" => 3, "required" => false],
            "TELFOU" => ["length" => 20, "required" => false],
            "FAXFOU" => ["length" => 20, "required" => false],
            "TLXFOU" => ["length" => 15, "required" => false],
            "EDIFOU" => ["length" => 14, "required" => true],
            "CODTRA" => ["length" => 14, "required" => false],
            "IGLSIT" => ["length" => 3, "required" => false],
            "CODTRE" => ["length" => 3, "required" => true],
            "GSTAVI" => ["length" => 1, "required" => false],
            "GSTRCP" => ["length" => 1, "required" => false],
            "TYPPRT" => ["length" => 1, "required" => true],
            "CPIFOU" => ["length" => 10, "required" => false],
            "EMBTIE" => ["length" => 1, "required" => false],
            "DISEXC" => ["length" => 5, "required" => false],
        ],
    ];

    public function getTemplateData()
    {
        return static::$templateData;
    }

    public function getValidationTemplateData()
    {
        return static::$validationTemplateData;
    }

    public function __toString()
    {
        return "M10";
    }
}
