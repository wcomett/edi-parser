<?php
/**
 * This file is part of the EdiParser package.
 *
 * @package     EdiParserBundle
 * @since       0.0.1
 * @author      davidbonachera
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Boda\EdiParserBundle\Model\Infolog;

use Boda\EdiParserBundle\Model\ModelAbstract;

class M40 extends ModelAbstract
{
    /**
     * Template Data
     *
     * @var array $templateData
     */
    public static $templateData = [
        // Déclaration produit (obligatoire)
        "40.00" => [
            "CODEXC" => "40",
            "SEPEXC" => ".",
            "SCOEXC" => "00",
            "TRTEXC" => null,
            "NUMREC" => null,
            "SNUREC" => null,
            "REFREC" => null,
            "CODAPP" => null,
            "CODLDR" => null,
            "CODTRE" => null,
            "CODACT" => null,
            "CODFOU" => null,
            "CODTRA" => "CARRIER",
            "DTIREC" => null,
            "HEIREC" => null,
            "KAIREC" => null,
            "MSGREC1" => null,
            "MSGREC2" => null,
            "IGLSIT" => null,
            "DTFREC" => null,
            "EDIFOU" => null,
            "DISEXC" => null,
        ],
        "40.20" => [
            "CODEXC" => "40",
            "SEPEXC" => ".",
            "SCOEXC" => "20",
            "TRTEXC" => null,
            "NUMREC" => null,
            "SNUREC" => null,
            "REFREC" => null,
            "NLIREC" => null,
            "CODACT" => null,
            "CODCLI" => null,
            "CODPRO" => null,
            "VALPRO" => null,
            "UVCREA" => null,
            "UNICDE" => null,
            "CODPRN" => null,
            "TYPOPE" => null,
            "NUMOPE" => null,
            "AIGPRI" => null,
            "PRIREC" => null,
            "MSGLIG" => null,
            "MOTIMM" => null,
            "CODLOT" => null,
            "DATFAB" => null,
            "DATFVI" => null,
            "SPCPRO" => null,
            "PCBPRO" => null,
            "PDBCOL" => null,
            "VOLCOL" => null,
            "COLCOU" => null,
            "COUPAL" => null,
            "CODEMB" => null,
            "GERPAL" => null,
            "GERHAU" => null,
            "CODMDR" => null,
            "IGLSIT" => null,
            "INDUNI" => null,
        ],
        "40.99" => [
            "CODEXC" => "40",
            "SEPEXC" => ".",
            "SCOEXC" => "99",
            "TRTEXC" => null,
            "NUMREC" => null,
            "SNUREC" => null,
            "REFREC" => null,
            "CUMLIG" => null,
            "DISEXC" => null,
        ],
    ];

    public static $validationTemplateData = [
        "40.00" => [
            "CODEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "SEPEXC" => ["length" => 1, "required" => true],
            "SCOEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "TRTEXC" => ["length" => 1, "required" => true],
            "NUMREC" => ["length" => 8, "required" => false, "numerical" => true],
            "SNUREC" => ["length" => 3, "required" => false, "numerical" => true],
            "REFREC" => ["length" => 30, "required" => true],
            "CODAPP" => ["length" => 10, "required" => false],
            "CODLDR" => ["length" => 3, "required" => false],
            "CODTRE" => ["length" => 3, "required" => false],
            "CODACT" => ["length" => 3, "required" => true],
            "CODFOU" => ["length" => 14, "required" => true],
            "CODTRA" => ["length" => 14, "required" => false],
            "DTIREC" => ["length" => 8, "required" => false, "numerical" => true],
            "HEIREC" => ["length" => 4, "required" => false, "numerical" => true],
            "KAIREC" => ["length" => 3, "required" => false, "numerical" => true],
            "MSGREC1" => ["length" => 30, "required" => false],
            "MSGREC2" => ["length" => 30, "required" => false],
            "IGLSIT" => ["length" => 3, "required" => false, "numerical" => true],
            "DTFREC" => ["length" => 8, "required" => false, "numerical" => true],
            "EDIFOU" => ["length" => 14, "required" => true],
            "DISEXC" => ["length" => 62, "required" => false],
        ],
        "40.20" => [
            "CODEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "SEPEXC" => ["length" => 1, "required" => true],
            "SCOEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "TRTEXC" => ["length" => 1, "required" => true],
            "NUMREC" => ["length" => 8, "required" => false, "numerical" => true],
            "SNUREC" => ["length" => 3, "required" => false, "numerical" => true],
            "REFREC" => ["length" => 30, "required" => true],
            "NLIREC" => ["length" => 5, "required" => true, "numerical" => true],
            "CODACT" => ["length" => 3, "required" => true],
            "CODCLI" => ["length" => 14, "required" => false],
            "CODPRO" => ["length" => 17, "required" => true],
            "VALPRO" => ["length" => 2, "required" => false, "numerical" => true],
            "UVCREA" => ["length" => 9, "required" => true, "numerical" => true],
            "UNICDE" => ["length" => 3, "required" => true],
            "CODPRN" => ["length" => 17, "required" => false],
            "TYPOPE" => ["length" => 1, "required" => false],
            "NUMOPE" => ["length" => 6, "required" => false, "numerical" => true],
            "AIGPRI" => ["length" => 1, "required" => false],
            "PRIREC" => ["length" => 9, "required" => false, "numerical" => true],
            "MSGLIG" => ["length" => 30, "required" => false],
            "MOTIMM" => ["length" => 3, "required" => false],
            "CODLOT" => ["length" => 20, "required" => false],
            "DATFAB" => ["length" => 8, "required" => false, "numerical" => true],
            "DATFVI" => ["length" => 8, "required" => false, "numerical" => true],
            "SPCPRO" => ["length" => 4, "required" => false, "numerical" => true],
            "PCBPRO" => ["length" => 5, "required" => false, "numerical" => true],
            "PDBCOL" => ["length" => 7, "required" => false, "numerical" => true],
            "VOLCOL" => ["length" => 7, "required" => false, "numerical" => true],
            "COLCOU" => ["length" => 8, "required" => false, "numerical" => true],
            "COUPAL" => ["length" => 8, "required" => false, "numerical" => true],
            "CODEMB" => ["length" => 3, "required" => false],
            "GERPAL" => ["length" => 2, "required" => false, "numerical" => true],
            "GERHAU" => ["length" => 2, "required" => false, "numerical" => true],
            "CODMDR" => ["length" => 3, "required" => true],
            "IGLSIT" => ["length" => 3, "required" => false, "numerical" => true],
            "INDUNI" => ["length" => 1, "required" => false],
        ],
        "40.99" => [
            "CODEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "SEPEXC" => ["length" => 1, "required" => true],
            "SCOEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "TRTEXC" => ["length" => 1, "required" => false],
            "NUMREC" => ["length" => 8, "required" => false, "numerical" => true],
            "SNUREC" => ["length" => 3, "required" => false, "numerical" => true],
            "REFREC" => ["length" => 30, "required" => true],
            "CUMLIG" => ["length" => 4, "required" => false, "numerical" => true],
            "DISEXC" => ["length" => 205, "required" => false],
        ],
    ];

    public function getTemplateData()
    {
        return static::$templateData;
    }

    public function getValidationTemplateData()
    {
        return static::$validationTemplateData;
    }

    public function __toString()
    {
        return "M40";
    }
}
