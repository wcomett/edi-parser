<?php
/**
 * This file is part of the EdiParser package.
 *
 * @package     EdiParserBundle
 * @since       0.0.1
 * @author      davidbonachera
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Boda\EdiParserBundle\Model\Infolog;

use Boda\EdiParserBundle\Model\ModelAbstract;

class M20 extends ModelAbstract
{
    /**
     * Template Data
     *
     * @var array $templateData
     */
    public static $templateData = [
        // Déclarations clients (obligatoire)
        "20.00" => [
            "CODEXC" => "20",
            "SEPEXC" => ".",
            "SCOEXC" => "00",
            "TRTEXC" => null,
            "CODACT" => null,
            "CODCLI" => null,
            "TYPCLI" => "1",
            "CATCLI" => "001",
            "CLILIV" => null,
            "EDICLI" => null,
            "NOMCLI" => null,
            "AD1CLI" => "AD1CLI",
            "AD2CLI" => null,
            "CPOCLI" => null,
            "VILCLI" => "VILCLI",
            "PAYCLI" => null,
            "TELCLI" => null,
            "FAXCLI" => null,
            "TLXCLI" => null,
            "CODLGE" => null,
            "CODDEV" => null,
            "IGLSIT" => null,
            "PTYCLI" => null,
            "ETACLI" => "20",
            "DISEXC" => null,
        ],

        // Declarations clients (Facultatif)
        "20.01" => [
            "CODEXC" => "20",
            "SEPEXC" => ".",
            "SCOEXC" => "01",
            "TRTEXC" => null,
            "CODACT" => null,
            "CODCLI" => null,
            "RSPCLI" => null,
            "CODMOP" => null,
            "CODRGT" => null,
            "CODTRA" => null,
            "TOULIV1" => null,
            "TOULIV2" => null,
            "TOULIV3" => null,
            "TOULIV4" => null,
            "TOULIV5" => null,
            "TOULIV6" => null,
            "TOULIV7" => null,
            "ORDLIV" => null,
            "KAILIV1" => null,
            "KAILIV2" => null,
            "KAILIV3" => null,
            "KAILIV4" => null,
            "KAILIV5" => null,
            "KAILIV6" => null,
            "KAILIV7" => null,
            "GSTALC" => null,
            "PTYDES" => null,
            "NUMACS" => null,
            "CPICLI" => null,
            "TVACCE" => null,
            "IDEALC" => null,
            "DISEXC" => null,
        ],

        // Déclaration clients (Partie 2:Facultatif)
        "20.02" => [
            "CODEXC" => "20",
            "SEPEXC" => ".",
            "SCOEXC" => "02",
            "TRTEXC" => null,
            "CODACT" => null,
            "CODCLI" => null,
            "CM1CLI" => null,
            "CM2CLI" => null,
            "CM3CLI" => null,
            "EMLADR" => null,
            "CPICLI" => null,
            "DISEXC" => null,
        ],
    ];

    public static $validationTemplateData = [
        "20.00" => [
            "CODEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "SEPEXC" => ["length" => 1, "required" => true],
            "SCOEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "TRTEXC" => ["length" => 1, "required" => true],
            "CODACT" => ["length" => 3, "required" => true],
            "CODCLI" => ["length" => 14, "required" => true],
            "TYPCLI" => ["length" => 1, "required" => false],
            "CATCLI" => ["length" => 3, "required" => true],
            "CLILIV" => ["length" => 14, "required" => false],
            "EDICLI" => ["length" => 14, "required" => false],
            "NOMCLI" => ["length" => 30, "required" => true],
            "AD1CLI" => ["length" => 30, "required" => true],
            "AD2CLI" => ["length" => 30, "required" => false],
            "CPOCLI" => ["length" => 5, "required" => false, "numerical" => true],
            "VILCLI" => ["length" => 26, "required" => true],
            "PAYCLI" => ["length" => 3, "required" => false],
            "TELCLI" => ["length" => 20, "required" => false],
            "FAXCLI" => ["length" => 20, "required" => false],
            "TLXCLI" => ["length" => 15, "required" => false],
            "CODLGE" => ["length" => 3, "required" => false],
            "CODDEV" => ["length" => 3, "required" => true],
            "IGLSIT" => ["length" => 3, "required" => false, "numerical" => true],
            "PTYCLI" => ["length" => 3, "required" => false],
            "ETACLI" => ["length" => 2, "required" => true],
            "DISEXC" => ["length" => 8, "required" => false],
        ],
        "20.01" => [
            "CODEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "SEPEXC" => ["length" => 1, "required" => true],
            "SCOEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "TRTEXC" => ["length" => 1, "required" => false],
            "CODACT" => ["length" => 3, "required" => true],
            "CODCLI" => ["length" => 14, "required" => true],
            "RSPCLI" => ["length" => 30, "required" => false],
            "CODMOP" => ["length" => 3, "required" => false],
            "CODRGT" => ["length" => 3, "required" => false],
            "CODTRA" => ["length" => 14, "required" => false],
            "TOULIV1" => ["length" => 4, "required" => false, "numerical" => true],
            "TOULIV2" => ["length" => 4, "required" => false, "numerical" => true],
            "TOULIV3" => ["length" => 4, "required" => false, "numerical" => true],
            "TOULIV4" => ["length" => 4, "required" => false, "numerical" => true],
            "TOULIV5" => ["length" => 4, "required" => false, "numerical" => true],
            "TOULIV6" => ["length" => 4, "required" => false, "numerical" => true],
            "TOULIV7" => ["length" => 4, "required" => false, "numerical" => true],
            "ORDLIV" => ["length" => 28, "required" => false, "numerical" => true],
            "KAILIV1" => ["length" => 3, "required" => false, "numerical" => true],
            "KAILIV2" => ["length" => 3, "required" => false, "numerical" => true],
            "KAILIV3" => ["length" => 3, "required" => false, "numerical" => true],
            "KAILIV4" => ["length" => 3, "required" => false, "numerical" => true],
            "KAILIV5" => ["length" => 3, "required" => false, "numerical" => true],
            "KAILIV6" => ["length" => 3, "required" => false, "numerical" => true],
            "KAILIV7" => ["length" => 3, "required" => false, "numerical" => true],
            "GSTALC" => ["length" => 1, "required" => false],
            "PTYDES" => ["length" => 2, "required" => false, "numerical" => true],
            "NUMACS" => ["length" => 20, "required" => false],
            "CPICLI" => ["length" => 9, "required" => false],
            "TVACCE" => ["length" => 20, "required" => false],
            "IDEALC" => ["length" => 20, "required" => false],
            "DISEXC" => ["length" => 34, "required" => false],
        ],
        "20.02" => [
            "CODEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "SEPEXC" => ["length" => 1, "required" => true],
            "SCOEXC" => ["length" => 2, "required" => true, "numerical" => true],
            "TRTEXC" => ["length" => 1, "required" => true],
            "CODACT" => ["length" => 3, "required" => true],
            "CODCLI" => ["length" => 14, "required" => true],
            "CM1CLI" => ["length" => 45, "required" => false],
            "CM2CLI" => ["length" => 45, "required" => false],
            "CM3CLI" => ["length" => 45, "required" => false],
            "EMLADR" => ["length" => 50, "required" => false],
            "CPICLI" => ["length" => 10, "required" => false],
            "DISEXC" => ["length" => 38, "required" => false],
        ],
    ];

    public function getTemplateData()
    {
        return static::$templateData;
    }

    public function getValidationTemplateData()
    {
        return static::$validationTemplateData;
    }

    public function __toString()
    {
        return "M20";
    }
}
